ERLANG Driver开发SDK简单版
================================

ERLANG端模块

~~~~
#!erlang

-module(test_driver).
-behaviour(erl_easy_driver).
-export([start_link/0, init/0, encode_msg/3, decode_msg/3]).
-define(SERVER, ?MODULE).

start_link() ->
    % 第3，4个参数分别是驱动所在的App和驱动名称
	erl_easy_driver:start_link({local, ?SERVER}, ?MODULE, test_driver, "TestDriver").

init() -> {ok, undefined}.

encode_msg(_MsgType, _Data, State) ->
	{<<1,2,3>>, State}.

decode_msg(_MsgType, Data, State) ->
	{[1,2,3], State}.

~~~~

C++端模块

需要在rebar.config配置erl_easy_driver.h到搜索路径中

~~~~
#!c++

#include <erl_easy_driver.h>

struct MyDriver : public erl::EasyDriver<MyDriver> {
    virtual void handleRequest(const char *data, size_t size) {
        reply("abcd", 4);
    }
};

ERL_EASY_DRIVER(MyDriver);

~~~~