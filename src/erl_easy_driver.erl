%% -*- coding: utf-8 -*-

-module(erl_easy_driver).
-export([start_link/4, call/3, init_it/3]).

-callback init() -> {ok, State :: term()} | {error, Reason :: term()}.

-callback encode_msg(MsgType :: any(), Data :: any(), State :: term()) -> {binary(), NewState :: term()}.

-callback decode_msg(MsgType :: any(), Binary :: binary(), State :: term()) -> {term(), NewState :: term()}.

-record(state, {
	user_state,
	mod
}).

%% @doc 启动端口服务
start_link(Name, Mod, DrvApp, DrvName) ->
	PrivPath = code:priv_dir(DrvApp),
	case erl_ddll:load_driver(PrivPath, DrvName) of
		ok -> ok;
		{error, already_loaded} -> ok;
		{error, ErrorDesc} -> throw({error, erl_ddll:format_error(ErrorDesc)})
	end,
	{ok, spawn_link(?MODULE, init_it, [Name, Mod, DrvName])}.

register_name({global, Name}, Pid) -> global:register_name(Name, Pid);
register_name({local, Name}, Pid) -> register(Name, Pid).

%% @hidden
init_it(Name, Mod, DrvName) ->
	register_name(Name, self()),
	Port = open_port({spawn, DrvName}, [binary]),
	{ok, UserState} = Mod:init(),
	State = #state{
		user_state = UserState,
		mod = Mod
	},
	loop(Port, State).

call(Server, MsgType, Data) ->
	Server ! {call, Server, self(), {MsgType, Data}},
	receive
		{Server, Result} ->
			Result
	after 5000 ->
		{error, timeout}
	end.

%% @private
loop(Port, State) ->
	receive
		{call, Server, Caller, {MsgType, Data}} ->
			Mod = State#state.mod,
			{MsgBinary, NewUserState} = Mod:encode_msg(MsgType, Data, State#state.user_state),
			Port ! {self(), {command, MsgBinary}},
			receive
				{Port, {data, RecvData}} ->
					Mod = State#state.mod,
					{Result, NewUserState2} = Mod:decode_msg(MsgType, RecvData, NewUserState),
					Caller ! {Server, Result},
					loop(Port, State#state{user_state = NewUserState2})
			end;
		stop ->
			Port ! {self(), close},
			receive
				{Port, closed} ->
					exit(normal)
			end;
		{'EXIT', Port, _Reason} ->
			exit(port_terminated)
	end.
