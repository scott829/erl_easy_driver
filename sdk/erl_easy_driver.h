#pragma once

#include <erl_driver.h>

namespace erl {
    template <class Class>
    struct EasyDriver {
        // 驱动返回数据的端口
        ErlDrvPort port;

        virtual ~EasyDriver() {}

        // 处理请求
        virtual void handleRequest(const char *data, size_t size) = 0;

        void reply(const char *data, size_t size) {
            driver_output(this->port, (char *)data, size);
        }

        // 驱动初始化
        static ErlDrvData start(ErlDrvPort port, char *command) {
            Class *context = new Class();
            context->port = port;
            return (ErlDrvData)context;
        }

        // 驱动停止
        static void stop(ErlDrvData data) {
            delete (Class *)data;
        }

        // 驱动处理请求函数
        static void output(ErlDrvData drv_data, char *buff, ErlDrvSizeT bufflen) {
            Class *context = (Class *)drv_data;
            context->handleRequest((const char *)buff, (size_t)bufflen);
        }
    };
}

#define ERL_EASY_DRIVER_2(ClassName, DriverName) \
    static ErlDrvEntry ClassName##_entry = { \
	    NULL, \
	    ClassName::start, \
	    ClassName::stop, \
	    ClassName::output, \
	    NULL, \
	    NULL, \
	    (char *)DriverName, \
	    NULL, \
	    NULL, \
	    NULL, \
	    NULL, \
	    NULL, \
	    NULL, \
	    NULL, \
	    NULL, \
	    NULL, \
	    ERL_DRV_EXTENDED_MARKER, \
	    ERL_DRV_EXTENDED_MAJOR_VERSION, \
	    ERL_DRV_EXTENDED_MINOR_VERSION, \
	    0, \
	    NULL, \
	    NULL, \
	    NULL, \
    }; \
    extern "C" { \
	DRIVER_INIT() { \
		return &ClassName##_entry; \
	} \
}

#define ERL_EASY_DRIVER(ClassName) ERL_EASY_DRIVER_2(ClassName, #ClassName)